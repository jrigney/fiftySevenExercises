module Lib
  ( someFunc
  ) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

values :: (Num a) => a -> a -> [String]
values x y =
  [ show (x :: Integer) ++ "+" ++ show y ++ "=" ++ show (x + y)
  , show (x :: Integer) ++ "-" ++ show y ++ "=" ++ show (x - y)
  , show (x :: Integer) ++ "*" ++ show y ++ "=" ++ show (x * y)
  ]
