module Main where

import Lib

output :: String -> String
output input
  | (length input) <= 0 = "Needs input"
  | otherwise = input ++ " has " ++ (show (length input)) ++ " chars"

main :: IO ()
main = do
  putStrLn "Enter input string"
  input <- getLine
  putStrLn $ (output input)
