module Main where

import Lib

special :: String -> String
special "bob" = "Hello Robert"
special "dick" = "Hello Richard"
special other = "Hello " ++ other

main :: IO ()
main = do
  putStrLn "Enter Name"
  name <- getLine
  putStrLn $ special name
