module Lib
  ( someFunc
  ) where

quotes = [("obi wan", "something droids"), ("bob", "mmm hamburger")]

asString :: [String]
asString = fmap (\(x, y) -> x ++ " says \"" ++ y ++ "\"") quotes

someFunc :: IO ()
someFunc = mapM_ putStrLn asString
