module Lib
  ( someFunc
  , story
  ) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

story :: [String] -> String
story xs = "Did you walk your " ++ xs !! 0 ++ " " ++ xs !! 1 ++ " " ++ xs !! 2
