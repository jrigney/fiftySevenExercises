{-# LANGUAGE OverloadedStrings #-}

module Main where

import Lib

main :: IO ()
main = do
  putStrLn "Adj"
  adj <- getLine
  putStrLn "Noun"
  noun <- getLine
  putStrLn "Adv"
  adv <- getLine
  putStrLn $ (story [adj, noun, adv])
